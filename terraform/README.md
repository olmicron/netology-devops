> В файле README.md опишите своими словами, какие файлы будут проигнорированы в будущем благодаря добавленному .gitignore.

_Не сказано где должен быть этот файл (который в корне, или здесь), по этому создал его здесь :)_

Будут проигнорированы все файлы указанные в `/terraform/.gitignore`, например, относительно директории `/terraform` (`/terraform` - считаем за корень):
 * `*.tfstate` - все файлы с "расширением" `.tfstate` в корне 
 * `*.tfstate.*` - все файлы в имени которых есть `.tfstate.` в корне 
 * `**/.terraform/*` - все файлы и директории, в директориях (включая поддиректории) с именем `.terraform`
 * `*_override.tf` - все файлы в корне, имя которых оканчивается на `_override.tf`

и т.п.

_П.С. под файлами также подразумеваю и директории, всё что относится к файлам - также относится и к директориям, соответственно игнорируются не только файлы, но и директории._
